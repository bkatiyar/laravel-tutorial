<?php

namespace App\Http\Controllers\Country;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CountryModel;
use Validator;

class CountryController extends Controller
{
    
    private $request;
    private $country;

    public function __construct(Request $request, CountryModel $country) {
        $this->request = $request;
        $this->country = $country;
    }

    public function country () {
        return response()->json(CountryModel::get(), 200);
    }

    public function countryByID ($id) {
        $country = CountryModel::find($id);
        if (is_null($country)) {
            return response()->json(["message" => "Record not found"], 404);
        }

        return response()->json($country, 200);
    }

    public function countrySave () {
        $rules = [
            'name'=> 'required | min:3',
            'iso' => 'required | min:2 | max:2'   
        ];
        $validator = Validator::make($this->request->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        
        $country = CountryModel::create($this->request->all());
        return response()->json($country, 201);
    }

    public function countryUpdate ($id) {
        $country = CountryModel::find($id);
        if (is_null($country)) {
            return response()->json(["message" => "Record not found"], 404);
        }
        $country->update($this->request->all());

        return response()->json($country, 200);
    }

    public function countryDelete ($id) {
        $country = CountryModel::find($id);
        if (is_null($country)) {
            return response()->json(["message" => "Record not found"], 404);
        }
        $country->delete();

        return response()->json(null, 204);
    }
}
